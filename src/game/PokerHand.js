import CardDeck from "./CardDeck";
import getCombinations from './Combinations';

export const OUTCOMES = {
  HIGH_CARD: {'id': 'High card', weight: '0'},
  PAIR: {'id': 'A Pair', weight: '1'},
  TWO_PAIRS: {'id': 'Two Pairs', weight: '2'},
  THREE_OF_A_KIND: {'id': 'Three of a Kind', weight: '3'},
  STRAIGHT: {'id': 'Straight', weight: '4'},
  FLUSH: {'id': 'Flush', weight: '5'},
  FULL_HOUSE: {'id': 'Full House', weight: '6'},
  FOUR_OF_A_KIND: {'id': 'Four of a Kind', weight: '7'},
  STRAIGHT_FLUSH: {'id': 'Straight Flush', weight: '8'},
  ROYAL_FLUSH: {'id': 'Royal Flush', weight: '9'}
};

const comparators = {
  [OUTCOMES.HIGH_CARD]: () => getRankWeight(this.params.rank).toString(16),
  [OUTCOMES.PAIR]: () => getRankWeight(this.params.rank).toString(16),
  [OUTCOMES.TWO_PAIRS]: () => {
    return getRankWeight(this.params.ranks[0]).toString(16) +
      getRankWeight(this.params.ranks[1]).toString(16);
  },
  [OUTCOMES.THREE_OF_A_KIND]: () => {
    return getRankWeight(this.params.rank).toString(16);
  },
  [OUTCOMES.STRAIGHT]: () => {
    return getRankWeight(this.params.rank).toString(16);
  },
  [OUTCOMES.FLUSH]: () => '',
  [OUTCOMES.FULL_HOUSE]: () => {
    return getRankWeight(this.params.highRank).toString(16) +
      getRankWeight(this.params.lowRank).toString(16);
  },
  [OUTCOMES.FOUR_OF_A_KIND]: () => {
    return getRankWeight(this.params.rank).toString(16);
  },
  [OUTCOMES.STRAIGHT_FLUSH]: () => {
    return getRankWeight(this.params.rank).toString(16);
  },
  [OUTCOMES.ROYAL_FLUSH]: () => ''
};

const stringifiers = {
  [OUTCOMES.HIGH_CARD.id]: function() { return OUTCOMES.HIGH_CARD.id + ` (${this.params.rank})` },
  [OUTCOMES.PAIR.id]: function() { return OUTCOMES.PAIR.id + ` (${this.params.rank})` },
  [OUTCOMES.TWO_PAIRS.id]: function() {
    return OUTCOMES.TWO_PAIRS.id + ` (${this.params.ranks[0]}, ${this.params.ranks[1]})`
  },
  [OUTCOMES.THREE_OF_A_KIND.id]: function() { return OUTCOMES.THREE_OF_A_KIND.id + ` (${this.params.rank})` },
  [OUTCOMES.STRAIGHT.id]: function() { return OUTCOMES.STRAIGHT.id + ` (${this.params.rank}) high` },
  [OUTCOMES.FLUSH.id]: function() { return OUTCOMES.FLUSH.id + ` (${this.params.suit})` },
  [OUTCOMES.FULL_HOUSE.id]: function() {
    return OUTCOMES.FULL_HOUSE.id + ` (${this.params.highRank}, ${this.params.lowRank})`
  },
  [OUTCOMES.FOUR_OF_A_KIND.id]: function() { return OUTCOMES.FOUR_OF_A_KIND.id + ` (${this.params.rank})` },
  [OUTCOMES.STRAIGHT_FLUSH.id]: function() { return OUTCOMES.STRAIGHT_FLUSH.id + ` (${this.params.rank} high, ${this.params.suit})` }
};

class Outcome {
  constructor(type, params) {
    this.type = type;
    this.params = params;
    this.comparableString = comparators[this.type].bind(this);
  }

  valueOf() {
    return this._createComparableValue();
  }

  toString() {
    return this._createComparableValue();
  }

  getTxt() {
    return stringifiers[this.type.id].call(this);
  }

  _createComparableValue() {
    return this.type.weight + this.comparableString;
  }
}

export const CARD_RANK_WEIGHTS = [
  CardDeck.RANKS.TWO,
  CardDeck.RANKS.THREE,
  CardDeck.RANKS.FOUR,
  CardDeck.RANKS.FIVE,
  CardDeck.RANKS.SIX,
  CardDeck.RANKS.SEVEN,
  CardDeck.RANKS.EIGHT,
  CardDeck.RANKS.NINE,
  CardDeck.RANKS.TEN,
  CardDeck.RANKS.JACK,
  CardDeck.RANKS.QUEEN,
  CardDeck.RANKS.KING,
  CardDeck.RANKS.ACE,
];

const last = (arr) => {
  return arr[arr.length - 1];
};

export const getRankWeight = (rank) => {
  return CARD_RANK_WEIGHTS.indexOf(rank);
};

class PokerHand {
  constructor(cards) {
    this.cards = [...cards];

    this.sortedCards = this.cards.sort((card1, card2) => {
      return getRankWeight(card1.rank) - getRankWeight(card2.rank);
    });

    this.ranksFreq = this.sortedCards.reduce((freq, card) => {
      const rankFreq = freq.get(card.rank);
      freq.set(card.rank, rankFreq ? rankFreq + 1 : 1);
      return freq;
    }, new Map());

    this.threes = [];
    this.pairs = [];
    this.four = null;

    for (let [rank, freq] of this.ranksFreq) {
      if (freq === 4) this.four = rank;
      if (freq === 3) this.threes.push(rank);
      if (freq === 2) this.pairs.push(rank);
    }

    this.outcomes = [];
  }

  _addOutcome(outcome) {
    this.outcomes.push(outcome);
  }

  _determinePossibleOutcomes() {
    const combinations = getCombinations(this.sortedCards, 5);

    for (let combo of combinations) {
      const isStraight = this.isStraight(combo);
      const isFlush = this.isFlushSet(combo);

      const lastCard = last(combo);
      if (isStraight && isFlush) {
        if (lastCard.rank === CardDeck.RANKS.ACE) {
          this._addOutcome(new Outcome(OUTCOMES.ROYAL_FLUSH, {suit: lastCard.suit}));
        } else {
          this._addOutcome(new Outcome(OUTCOMES.STRAIGHT_FLUSH, {rank: lastCard.rank, suit: lastCard.suit}));
        }
      } else if (isStraight) {
        this._addOutcome(new Outcome(OUTCOMES.STRAIGHT, {rank: lastCard.rank}));
      } else if (isFlush) {
        this._addOutcome(new Outcome(OUTCOMES.FLUSH, {suit: lastCard.suit}));
      }
    }

    if (this.four) {
      this._addOutcome(new Outcome(OUTCOMES.FOUR_OF_A_KIND, {rank: this.four}));
    } else if (this.threes.length >= 1) {
      const highRank = last(this.threes);

      if (this.threes.length >= 2 || this.pairs.length >= 1) {
        const remainingThrees = [...this.threes];
        remainingThrees.pop();
        const remainingRanks = remainingThrees.concat(this.pairs);
        remainingRanks.sort((rank1, rank2) => {
          return getRankWeight(rank1) - getRankWeight(rank2);
        });

        const lowRank = last(remainingRanks);
        this._addOutcome(new Outcome(OUTCOMES.FULL_HOUSE, {highRank, lowRank}));
      } else {
        this._addOutcome(new Outcome(OUTCOMES.THREE_OF_A_KIND, {rank: highRank}));
      }
    } else if (this.pairs.length >= 2) {
      this._addOutcome(new Outcome(OUTCOMES.TWO_PAIRS, {ranks: this.pairs.slice(-2)}));
    } else if (this.pairs.length === 1) {
      this._addOutcome(new Outcome(OUTCOMES.PAIR, {rank: this.pairs[0]}));
    } else {
      this._addOutcome(new Outcome(OUTCOMES.HIGH_CARD, {rank: last(this.sortedCards).rank}));
    }
  }

  isFlushSet(cards) {
    return cards.every(card => card.suit === cards[0].suit);
  }

  isStraight(cards) {
    let sequenceLength = 1;
    let isStraight = cards.every((currCard, index) => {
      if (index === 0) return true;
      const prevCard = cards[index - 1];
      const weightDiff = getRankWeight(currCard.rank) - getRankWeight(prevCard.rank);
      if (weightDiff === 1) {
        sequenceLength++;
        return true;
      }
      return false;
    });

    if (sequenceLength === 4 &&
      cards[3].rank === CardDeck.RANKS.FIVE &&
      cards[4].rank === CardDeck.RANKS.ACE
    ) {
      cards.unshift(cards.pop());
      isStraight = true;
    }

    return isStraight;
  }

  getOutcome() {
    this._determinePossibleOutcomes();

    this.outcomes.sort();

    return last(this.outcomes);
  }
}

export default PokerHand;