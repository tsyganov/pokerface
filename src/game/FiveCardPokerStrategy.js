import PokerHand, {getRankWeight, OUTCOMES} from "./PokerHand";
import CardDeck from './CardDeck';
import combinations from "./Combinations";

const allAreSameSuits = cards => cards.every(card => card.suit === cards[0].suit);

const incompleteSequence = (cards, maxWeight) => {
  if (!maxWeight) maxWeight = getRankWeight(cards[cards.length - 1].rank);
  return cards.every(card => {
    return getRankWeight(card.rank) > maxWeight - (cards.length + 1);
  });
};

const highRanks = [
  CardDeck.RANKS.JACK,
  CardDeck.RANKS.QUEEN,
  CardDeck.RANKS.KING,
  CardDeck.RANKS.ACE
];

class Combination {
  constructor(cards, sortedCards, outcome) {
    this.cards = cards;
    this.sortedCards = sortedCards;
    this.outcome = outcome;
    this.discarded = [];
  }

  getDiscarded() {
    return this.discarded;
  }
}

class RoyalFlush extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.ROYAL_FLUSH;
  }
}

class StraightFlush extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.STRAIGHT_FLUSH;
  }
}

class FourOfAKind extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.FOUR_OF_A_KIND;
  }

  getDiscarded() {
    const fourOfAKindRank = this.outcome.params.rank;
    const unmatchedRankIndex = this.cards.findIndex(card => card.rank !== fourOfAKindRank);
    return [unmatchedRankIndex];
  }
}

class FourToARoyalFlush extends Combination {
  check() {
    const combos = combinations(this.cards, 4);

    return combos.some(combo => {
      const aceWeight = getRankWeight(CardDeck.RANKS.ACE);
      // aceWeight = 12, royal flush weights: 12, 11, 10, 9, 8.
      // So, every card in possible royal flush should have weight
      // more than aceWeight - 5
      const fourCardsToRoyalStraight = incompleteSequence(combo, aceWeight);

      if (fourCardsToRoyalStraight && allAreSameSuits(combo)) {
        this.cards.forEach((card, index) => {
          if (!combo.includes(card)) {
            this.discarded.push(index);
          }
        });

        return true;
      }

      return false;
    });
  }
}


class FullHouse extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.FULL_HOUSE;
  }
}

class Flush extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.FLUSH;
  }
}

class Straight extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.STRAIGHT;
  }
}

class ThreeOfAKind extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.THREE_OF_A_KIND;
  }

  getDiscarded() {
    const threeRank = this.outcome.params.rank;

    this.cards.forEach((card, index) => {
      if (card.rank !== threeRank) this.discarded.push(index);
    });

    return this.discarded;
  }
}

class FourToAStraightFlush extends Combination {
  check() {
    const combos = combinations(this.sortedCards, 4);

    return combos.some(combo => {
      let lastRank = combo[3].rank;
      let lastWeight = getRankWeight(lastRank);

      let fourCardsToStraight = incompleteSequence(combo, lastWeight);

      if (!fourCardsToStraight && lastRank === CardDeck.RANKS.ACE) {
        let remainingCards = [...combo];
        remainingCards.pop(); // remove ace from the end
        const comboWeights = remainingCards.map(card => getRankWeight(card.rank));
        const cardsLessThanSix = comboWeights.every(weight => weight < getRankWeight(CardDeck.RANKS.SIX));
        if (cardsLessThanSix) {
          lastWeight = getRankWeight(remainingCards[2].rank);
          fourCardsToStraight = incompleteSequence(remainingCards, lastWeight);
        }
      }

      if (fourCardsToStraight && allAreSameSuits(combo)) {
        this.cards.forEach((card, index) => {
          if (!combo.includes(card)) {
            this.discarded.push(index);
          }
        });

        return true;
      }

      return false;
    });
  }
}

class TwoPairs extends Combination {
  check() {
    return this.outcome.type === OUTCOMES.TWO_PAIRS;
  }

  getDiscarded() {
    const unmatchedRankIndex = this.cards.findIndex(card => !this.outcome.params.ranks.includes(card.rank));
    return [unmatchedRankIndex];
  }
}

class HighPair extends Combination {
  check() {
    const pairRank = this.outcome.params.rank;
    if (this.outcome.type === OUTCOMES.PAIR && highRanks.includes(pairRank)) {
      this.cards.forEach((card, index) => {
        if (card.rank !== pairRank) {
          this.discarded.push(index);
        }
      });

      return true;
    }

    return false;
  }
}

class ThreeCardsToRoyalFlush extends Combination {
  check() {
    const combos = combinations(this.sortedCards, 3);

    return combos.some(combo => {
      if (allAreSameSuits(combo)) {
        const aceWeight = getRankWeight(CardDeck.RANKS.ACE);
        // aceWeight = 12, royal flush weights: 12, 11, 10, 9, 8.
        // So, every card in possible royal flush should have weight
        // more than aceWeight - 5

        const cardsToRoyalFlush = combo.every(card => {
          return getRankWeight(card.rank) > aceWeight - 5;
        });

        if (cardsToRoyalFlush) {
          this.cards.forEach((card, index) => {
            if (!combo.includes(card)) {
              this.discarded.push(index);
            }
          });

          return true;
        }
      }

      return false;
    });
  }
}

class FourToAFlush extends Combination {
  check() {
    const combos = combinations(this.sortedCards, 4);

    return combos.some(combo => {
      if (allAreSameSuits(combo)) {
        this.cards.forEach((card, index) => {
          if (!combo.includes(card)) {
            this.discarded.push(index);
          }
        });

        return true;
      }

      return false;
    });
  }
}

class LowPair extends Combination {
  check() {
    const pairRank = this.outcome.params.rank;
    if (this.outcome.type === OUTCOMES.PAIR && !highRanks.includes(pairRank)) {
      this.cards.forEach((card, index) => {
        if (card.rank !== pairRank) {
          this.discarded.push(index);
        }
      });

      return true;
    }

    return false;
  }
}

class FourToOutsideStraight extends Combination {
  check() {
    const combos = combinations(this.sortedCards, 4);

    return combos.some(combo => {
      let outsideStraight = combo.every((currCard, index) => {
        if (index === 0) return true;
        let prevCard = combo[index - 1];
        if (getRankWeight(currCard.rank) - getRankWeight(prevCard.rank) === 1) {
          return true;
        }
      });

      if (outsideStraight) {
        if (combo[combo.length - 1].rank === CardDeck.RANKS.ACE) {
          // outside straight with ace at the end is an inside straight
          return false;
        }

        this.cards.forEach((card, index) => {
          if (!combo.includes(card)) {
            this.discarded.push(index);
          }
        });
        return true;
      }

      return false;
    });
  }
}

class TwoSuitedHighCards extends Combination {
  check() {
    let highCards = this.cards.filter(card => {
      return getRankWeight(card.rank) >= getRankWeight(CardDeck.RANKS.JACK);
    });

    let combos = combinations(highCards, 2);
    return combos.some(combo => {
      if (allAreSameSuits(combo)) {
        this.cards.forEach((card, index) => {
          if (!combo.includes(card)) {
            this.discarded.push(index);
          }
        });

        return true;
      }
    });
  }
}

class ThreeToStraightFlush extends Combination {
  check() {
    const combos = combinations(this.sortedCards, 3);

    return combos.some(combo => {
      if (allAreSameSuits(combo)) {
        let weights = combo.map(card => getRankWeight(card.rank));
        // if we take some incomplete  straight, like
        // 3 5 7 -> it could be 3 4 5 6 7
        // if we get weight diffs, we get 2 + 2 = 4
        // 3 4 5 => could be A 2 3 4 5 or 3 4 5 6 7 etc
        // weight diffs: 1 + 1 = 2
        // 3 6 7 = weight diffs: 3 + 1 = 4
        // 3 4 6 = weight diffs: 1 + 2 = 3, still is incomplete straight
        // so, weight diffs will be no more than 4
        // if we have ace:
        // 2 3 A
        // we should remove ace and put -1 weight at the beginning:
        // -1 0 1
        // weight diff (abs) is 2
        // so, weight diff should not be less than 2 and more than 4
        if (combo[combo.length - 1].rank === CardDeck.RANKS.ACE) {
          weights.pop();
          weights.unshift(-1);
        }
        // now we get the sum of deltas:
        const delta = weights.reduce((acc, val, idx, arr) => {
          if (idx === 0) return 0;
          return acc + (val - arr[idx - 1]);
        }, 0);

        if (delta >= 2 && delta <= 4) {
          this.cards.forEach((card, index) => {
            if (!combo.includes(card)) {
              this.discarded.push(index);
            }
          });

          return true;
        }

        return false;
      }
    });
  }
}

class TwoUnsuitedHighCards extends Combination {
  check() {
    let highCards = this.sortedCards.filter(card => {
      return getRankWeight(card.rank) >= getRankWeight(CardDeck.RANKS.JACK);
    });

    if (highCards.length >= 2) {
      highCards.splice(2, highCards.length);
      this.cards.forEach((card, index) => {
        if (!highCards.includes(card)) {
          this.discarded.push(index);
        }
      });
      return true;
    }

    return false;
  }
}

class OneHighCard extends Combination {
  check() {
    let highCardIndex = this.cards.findIndex(card => {
      return getRankWeight(card.rank) >= getRankWeight(CardDeck.RANKS.JACK);
    });

    if (highCardIndex !== -1) {
      this.discarded = [...this.cards.keys()];
      this.discarded.splice(highCardIndex, 1);
      return true;
    } else {
      return false;
    }
  }
}

class SuitedTenAndHighCard extends Combination {
  check() {
    let ten = this.cards.find(card => {
      return card.rank === CardDeck.RANKS.TEN;
    });

    let highCard = this.cards.find(card => {
      return highRanks.includes(card.rank);
    });

    if (ten && highCard) {
      this.cards.forEach((card, index) => {
        if (card !== ten && card !== highCard) {
          this.discarded.push(index);
        }
      });
      return true;
    }
    return false;
  }
}

class FiveCardPokerStrategy {
  constructor(cards) {
    this.cards = cards;
    this.sortedCards = this._createSortedCards();
    this.hand = new PokerHand(cards);
    this.outcome = this.hand.getOutcome();

    this.strategyTable = [
      RoyalFlush,
      StraightFlush,
      FourOfAKind,
      FourToARoyalFlush,
      FullHouse,
      Flush,
      Straight,
      ThreeOfAKind,
      FourToAStraightFlush,
      TwoPairs,
      HighPair,
      ThreeCardsToRoyalFlush,
      FourToAFlush,
      LowPair,
      FourToOutsideStraight,
      TwoSuitedHighCards,
      ThreeToStraightFlush,
      TwoUnsuitedHighCards,
      SuitedTenAndHighCard,
      OneHighCard
    ];
  }

  _createSortedCards() {
    const sortedCards = [...this.cards];
    sortedCards.sort((card1, card2) => {
      return getRankWeight(card1.rank) - getRankWeight(card2.rank);
    });

    return sortedCards;
  }

  whatToDiscard() {
    for (let strategy of this.strategyTable) {
      const combination = new strategy(this.cards, this.sortedCards, this.outcome);
      if (combination.check()) {
        return combination.getDiscarded();
      }
    }

    return [...this.cards.keys()];
  }
}

export default FiveCardPokerStrategy;