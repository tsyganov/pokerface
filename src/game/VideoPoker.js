import {CARD_RANK_WEIGHTS, OUTCOMES} from "./PokerHand";
import CardDeck from "./CardDeck";

const winTable = [
  {
    title: 'Jacks or better', outcome: OUTCOMES.PAIR, winMultiplier: 1, addCheck: params => {
      return CARD_RANK_WEIGHTS.indexOf(params.rank) >= CARD_RANK_WEIGHTS.indexOf(CardDeck.RANKS.JACK);
    }
  },
  {outcome: OUTCOMES.TWO_PAIRS, winMultiplier: 2},
  {outcome: OUTCOMES.THREE_OF_A_KIND, winMultiplier: 3},
  {outcome: OUTCOMES.STRAIGHT, winMultiplier: 4},
  {outcome: OUTCOMES.FLUSH, winMultiplier: 6},
  {outcome: OUTCOMES.FULL_HOUSE, winMultiplier: 9},
  {outcome: OUTCOMES.FOUR_OF_A_KIND, winMultiplier: 25},
  {outcome: OUTCOMES.STRAIGHT_FLUSH, winMultiplier: 50},
  {outcome: OUTCOMES.ROYAL_FLUSH, winMultiplier: 250, addMult: coins => coins === 5 ? 4 : 1},
];

winTable.reverse();

export const getWinAmount = (bet, outcome) => {
  const winItem = winTable.find(winItem => winItem.outcome.id === outcome.type.id);

  let currentWin = 0;

  if (winItem && (winItem.addCheck ? winItem.addCheck(outcome.params) : true)) {
    currentWin = bet
      * winItem.winMultiplier
      * (winItem.addMult ? winItem.addMult() : 1);
  }

  return currentWin;
};