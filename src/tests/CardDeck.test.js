import CardDeck from "../game/CardDeck";

const cardIsNotInDeck = function (deck, testCard) {
    return deck.cards.every(deckCard => (
        !(deckCard.rank === testCard.rank && deckCard.suit === testCard.suit)
    ));
};

it('creates a new deck', () => {
    const deck = new CardDeck();

    expect(deck.cards.length).toBe(52);
});

it('gives us a deck with all different cards', () => {
    const deck = new CardDeck();

    const testCards = [];

    for (let card of deck.cards) {
        let cardDoesNotRepeat = testCards.every(testCard => (
            !(testCard.rank === card.rank && testCard.suit === card.suit)
        ));

        if (cardDoesNotRepeat) {
            testCards.push(card);
        }
    }

    expect(testCards.length).toBe(52);
});

it('can give us random cards from a deck', () => {
    const deck = new CardDeck();

    let testCard = deck.getCard();

    expect(deck.cards.length).toBe(51);

    expect(cardIsNotInDeck(deck, testCard)).toBeTruthy();

    testCard = deck.getCard();

    expect(deck.cards.length).toBe(50);

    expect(cardIsNotInDeck(deck, testCard)).toBeTruthy();
});


it('can give as an arbitrary number of cards ', () => {
    const deck = new CardDeck();

    let cards = deck.getCards(5);

    expect(deck.cards.length).toBe(52 - 5);
    expect(cards.length).toBe(5);
});