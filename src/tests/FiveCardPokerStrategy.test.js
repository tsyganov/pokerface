/*
= Advanced Strategy Chart 99.54% Payback =
1. Dealt royal flush (800.0000)
2. Dealt straight flush (50.0000)
3. Dealt four of a kind (25.0000)
4. 4 to a royal flush (18.3617)
5. Dealt full house (9.0000)
Dealt flush (6.0000)
3 of a kind (4.3025)
Dealt straight (4.0000)
4 to a straight flush (3.5319)
Two pair (2.59574)
High pair (1.5365)
3 to a royal flush (1.2868) A
4 to a flush (1.2766)
Unsuited TJQK(0.8723)
Low pair (0.8237)
4 to an outside straight with 0-2 high cards(0.6809)
3 to a straight flush (type 1) (0.6207 to 0.6429)
Suited QJ (0.6004)B
4 to an inside straight, 4 high cards (0.5957)
Suited KQ or KJ (0.5821)
Suited AK, AQ, or AJ (0.5678)
4 to an inside straight, 3 high cards (0.5319)
3 to a straight flush (type 2) (0.5227 to 0.5097)C
Unsuited JQK (0.5005)
Unsuited JQ (0.4980)
Suited TJ (0.4968) D
2 unsuited high cards king highest (0.4862)
Suited TQ (0.4825) E
2 unsuited high cards ace highest (0.4743)
J only (0.4713)
Suited TK (0.4682) F
Q only (0.4681)
K only (0.4649)
A only (0.4640)
3 to a straight flush (type 3) (0.4431)
Garbage, discard everything (0.3597)
*/

/*
= SIMPLE STRATEGY =
royal flush
straight flush,
Four of a kind,
4 to a royal flush
full house
flush
straight
Three of a kind
4 to a straight flush
Two pair
High pair
3 to a royal flush
4 to a flush
Low pair
4 to an outside straight
2 suited high cards
3 to a straight flush
2 unsuited high cards (if more than 2 then pick the lowest 2)
Suited 10/J, 10/Q, or 10/K
One high card
Discard everything
 */

import {set} from './assets/utils';
import FiveCardPokerStrategy from "../game/FiveCardPokerStrategy";
import CardDeck from "../game/CardDeck";
import PokerHand from "../game/PokerHand";
import {getWinAmount} from "../game/VideoPoker";

const nothing = [];
const first = 0;
const second = 1;
const third = 2;
const fourth = 3;
const fifth = 4;

const controlExamples = [
  // set of cards  , resultId        , which cards to discard
  ['DT DJ DQ DK DA', 'Royal flush', nothing],
  ['C5 C6 C7 C8 C9', 'Straight flush', nothing],
  ['D2 C2 CK S2 H2', 'Four of a Kind', [third]],
  ['DT C2 DJ DK DA', '4 to a royal flush', [second]],
  ['C2 D2 S2 DK HK', 'Full house', nothing],
  ['H3 H5 H7 H9 HA', 'Flush', nothing],
  ['C4 C5 H6 D7 S8', 'Straight', nothing],
  ['CJ HJ DJ SK D2', 'Three of a kind', [fourth, fifth]],
  ['C4 DK C6 C7 C8', '4 cards to a straight flush', [second]],
  ['CA C2 DK C3 C4', '4 cards to A-5 straight flush ', [third]],
  ['C5 C2 D5 HK H2', 'Two pairs', [fourth]],
  ['H5 CA D6 HA S3', 'High pair A A', [first, third, fifth]],
  ['HJ C5 D6 HA SJ', 'High pair J J', [second, third, fourth]],
  ['HT S3 HJ HA S5', '3 to a royal flush', [second, fifth]],
  ['ST SJ SQ HA S5', '3 to a royal flush', [fourth, fifth]],
  ['S2 S5 C4 ST SA', '4 to a flush', [third]],
  ['SJ CK H9 S5 C5', 'Low pair', [first, second, third]],
  ['S5 C6 D7 HA C8', '4 to outside straight', [fourth]],
  ['C2 S3 D4 HT C5', '4 to outside A-5 straight', [fourth]],
  ['CJ S3 CK HT C5', '2 suited high cards', [second, fourth, fifth]],
  ['C4 S2 C6 C7 HT', '3 to a straight flush', [second, fifth]],
  ['C4 S2 CA C3 HT', '3 to an A-5 straight flush', [second, fifth]],
  ['CK SJ C6 D7 D2', '2 unsuited high cards', [third, fourth, fifth]],
  ['CK SJ DQ D7 S2', '2 unsuited high cards (pick lowest two)', [first, fourth, fifth]],
  ['CT CJ D5 D6 S9', 'Suited 10/J', [third, fourth, fifth]],
  ['CT D4 C5 D6 CQ', 'Suited 10/Q', [second, third, fourth]],
  ['DK S5 C2 D6 DT', 'Suited 10/K', [second, third, fourth]],
  ['DJ D2 C5 S7 H8', 'High card', [second, third, fourth, fifth]]
];

controlExamples.forEach(example => {
  let [cards, resultId, expectedDiscarded] = example;
  cards = set(cards);
  const strategy = new FiveCardPokerStrategy(cards);
  const discarded = strategy.whatToDiscard();

  test('Poker strategy id = ' + resultId, () => {
    expect(discarded).toEqual(expectedDiscarded);
  });
});

const runCheck = () => {
  test('Poker strategy game test', () => {
    let credits = 100;
    let games = 10000;
    let totalWin = 0;
    const beforeStats = {};
    const afterStats = {};
    for (let i = 0; i < games; i++) {
      let bet = 5; // Betting max
      credits -= bet;
      const cardDeck = new CardDeck();
      let cards = cardDeck.getCards(5);
      let strategy = new FiveCardPokerStrategy(cards);
      let discarded = strategy.whatToDiscard();
      let outcomeId = strategy.outcome.type.id;

      !beforeStats[outcomeId] ? beforeStats[outcomeId] = 1 : beforeStats[outcomeId]++;

      for (let disIndex of discarded) {
        cards.splice(disIndex, 1, cardDeck.getCard());
      }

      let hand = new PokerHand(cards);
      let outcome = hand.getOutcome();
      outcomeId = outcome.type.id;
      !afterStats[outcomeId] ? afterStats[outcomeId] = 1 : afterStats[outcomeId]++;
      let win = getWinAmount(bet, outcome);
      credits += win;
      totalWin += win;
    }

    console.log('Games played: ', games);
    console.log('Credits left: ', credits);
    console.log('Total win: ', totalWin);
    console.log('before', beforeStats);
    console.log('after', afterStats);
  });
};

runCheck();