import * as controlExamples from "./controlExamples";

const createCard = strCard => {
  const suit = controlExamples.SUITS_MAP[strCard[0]];
  const rank = controlExamples.RANKS_MAP[strCard[1]];
  return {suit, rank};
};

export const set = cards => {
  return cards.split(' ').map(txtCard => createCard(txtCard));
};