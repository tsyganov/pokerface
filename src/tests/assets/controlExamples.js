import {OUTCOMES} from "../../game/PokerHand";
import CardDeck from "../../game/CardDeck";

export const RANKS_MAP = {
  '2': CardDeck.RANKS.TWO,
  '3': CardDeck.RANKS.THREE,
  '4': CardDeck.RANKS.FOUR,
  '5': CardDeck.RANKS.FIVE,
  '6': CardDeck.RANKS.SIX,
  '7': CardDeck.RANKS.SEVEN,
  '8': CardDeck.RANKS.EIGHT,
  '9': CardDeck.RANKS.NINE,
  'T': CardDeck.RANKS.TEN,
  'J': CardDeck.RANKS.JACK,
  'Q': CardDeck.RANKS.QUEEN,
  'K': CardDeck.RANKS.KING,
  'A': CardDeck.RANKS.ACE,
};

export const SUITS_MAP = {
  'D': CardDeck.SUITS.DIAMONDS,
  'H': CardDeck.SUITS.HEARTS,
  'C': CardDeck.SUITS.CLUBS,
  'S': CardDeck.SUITS.SPADES,
};

export const examples = [
  ['CA S7 H6 C8 DA SK H2',  OUTCOMES.PAIR,            '7c Pair'],
  ['C6 S7 H6 C8 DA',        OUTCOMES.PAIR,            '5c Pair'],
  ['C6 S6 HA CA C8 D8 D9',  OUTCOMES.TWO_PAIRS,       '7c Several pairs'],
  ['D5 S5 HK CK C8',        OUTCOMES.TWO_PAIRS,       '5c Two pairs'],
  ['D5 S5 H5 HK C8',        OUTCOMES.THREE_OF_A_KIND, '5c Three of a kind'],
  ['D4 S4 C4 HK CK',        OUTCOMES.FULL_HOUSE,      '5c Full house: 1 x 3, 1 x 2'],
  ['D4 S4 C4 HK CK D7 S7',  OUTCOMES.FULL_HOUSE,      '7c Full house: 1 x 3, 2 x 2'],
  ['D4 S4 C4 HK CK DK S7',  OUTCOMES.FULL_HOUSE,      '7c Full house: 2 x 3'],
  ['CA C2 C3 C4 C5 D8 SK',  OUTCOMES.STRAIGHT_FLUSH,  '7c Straight flush: A to 5'],
  ['DA D2 D3 D4 D5',        OUTCOMES.STRAIGHT_FLUSH,  '5c Straight flush: A to 5'],
  ['D2 D3 D4 D5 D6 C2 CK',  OUTCOMES.STRAIGHT_FLUSH,  '7c Straight flush D'],
  ['H2 H3 H4 H5 H6 D2 DK',  OUTCOMES.STRAIGHT_FLUSH,  '7c Straight flush H'],
  ['C2 C3 C4 C5 C6 S2 SK',  OUTCOMES.STRAIGHT_FLUSH,  '7c Straight flush C'],
  ['S2 S3 S4 S5 S6 C2 CK',  OUTCOMES.STRAIGHT_FLUSH,  '7c Straight flush S'],
  ['D2 D3 D4 D5 D6',        OUTCOMES.STRAIGHT_FLUSH,  '5c Straight flush D'],
  ['H2 H3 H4 H5 H6',        OUTCOMES.STRAIGHT_FLUSH,  '5c Straight flush H'],
  ['C2 C3 C4 C5 C6',        OUTCOMES.STRAIGHT_FLUSH,  '5c Straight flush C'],
  ['S2 S3 S4 S5 S6',        OUTCOMES.STRAIGHT_FLUSH,  '5c Straight flush S'],
  ['DT D2 D5 D4 D8 D9 DK',  OUTCOMES.FLUSH,           '7c Flush, all same suit'],
  ['HT H2 H5 H4 H8 H9 CK',  OUTCOMES.FLUSH,           '7c Flush'],
  ['CT C2 C5 C4 C8 D9 SK',  OUTCOMES.FLUSH,           '7c Flush'],
  ['ST S2 S5 S4 S8 SA D3',  OUTCOMES.FLUSH,           '7c Flush'],
  ['DT D2 D5 D4 D8',        OUTCOMES.FLUSH,           '5c Flush'],
  ['HT H2 H5 H4 H8',        OUTCOMES.FLUSH,           '5c Flush'],
  ['CT C2 C5 C4 C8',        OUTCOMES.FLUSH,           '5c Flush'],
  ['ST S2 S5 S4 S8',        OUTCOMES.FLUSH,           '5c Flush'],
  ['DT DJ DQ DK DA C9 D7',  OUTCOMES.ROYAL_FLUSH,     '7c Royal Flush'],
  ['HT HJ HQ HK HA C9 D7',  OUTCOMES.ROYAL_FLUSH,     '7c Royal Flush'],
  ['CT CJ CQ CK CA C9 D7',  OUTCOMES.ROYAL_FLUSH,     '7c Royal Flush'],
  ['ST SJ SQ SK SA C9 D7',  OUTCOMES.ROYAL_FLUSH,     '7c Royal Flush'],
  ['DT DJ DQ DK DA',        OUTCOMES.ROYAL_FLUSH,     '5c Royal Flush'],
  ['HT HJ HQ HK HA',        OUTCOMES.ROYAL_FLUSH,     '5c Royal Flush'],
  ['CT CJ CQ CK CA',        OUTCOMES.ROYAL_FLUSH,     '5c Royal Flush'],
  ['ST SJ SQ SK SA',        OUTCOMES.ROYAL_FLUSH,     '5c Royal Flush'],
];