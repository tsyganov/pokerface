import CardDeck from "../game/CardDeck";
import WinCalculator from "../game/PokerHand";

const {
  performance
} = require('perf_hooks');

const number = 100000;

const doLoadTesting = () => {
  const startTime = performance.now();
  const outcomes = [];

  for (let i = 0; i < number; i++) {
    const deck = new CardDeck();
    const cards = deck.getCards(7);
    const winCalc = new WinCalculator(cards);
    outcomes.push(winCalc.getOutcome());
  }

  const endTime = performance.now();
  const combinationsFrequency = outcomes.reduce((acc, outcome) => {
    const key = outcome.type.id;
    !acc[key] ? acc[key] = 1 : acc[key]++;
    return acc;
  }, {});

  console.log(combinationsFrequency);
  const totalTime = (endTime - startTime);
  console.log('Total time for ' + number + ' of calculations is: ' + totalTime);
  console.log('Avg time for one calculation: ' + (totalTime / number));
};

it('should do load testing', () => {
  // doLoadTesting();
  expect(true).toBe(true);
});
